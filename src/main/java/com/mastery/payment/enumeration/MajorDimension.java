package com.mastery.payment.enumeration;

public enum MajorDimension {
    ROWS, COLUMNS
}
