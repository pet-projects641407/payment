package com.mastery.payment.enumeration;

public enum InlineKeyboardCallbackData {
    FILL_TO_GOOGLE_SHEET,
    BACK_TO_MAIN_MENU,
    SHOW_MONEY_TRANSFER_INFO,
    STATISTIC_CURRENT_MONTH_FUNDS,
    STATISTIC_LAST_MONTH_SPENDING,
    STATISTIC_TOTAL_FUNDS,
    EXIT_MENU,

    NEW_SHEET_CREATION,

    // IncomeTextMessage confirmation
    APARTMENT_FUND_YES,
    APARTMENT_FUND_NO,
}
