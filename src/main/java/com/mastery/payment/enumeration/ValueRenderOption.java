package com.mastery.payment.enumeration;

public enum ValueRenderOption {
    FORMATTED_VALUE,
    UNFORMATTED_VALUE,
    FORMULA
}
