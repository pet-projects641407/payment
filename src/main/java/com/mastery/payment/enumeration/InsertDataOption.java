package com.mastery.payment.enumeration;

public enum InsertDataOption {
    OVERWRITE,
    INSERT_ROWS
}