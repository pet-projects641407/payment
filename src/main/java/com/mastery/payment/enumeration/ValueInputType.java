package com.mastery.payment.enumeration;

public enum ValueInputType {
    RAW,
    USER_ENTERED
}
