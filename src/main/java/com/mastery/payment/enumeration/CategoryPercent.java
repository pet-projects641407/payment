package com.mastery.payment.enumeration;

public enum CategoryPercent {
    EMERGENCY_FUND,
    SAVING_FUND,
    INVESTMENT_FUND,
    PERSONAL_FUND
}
