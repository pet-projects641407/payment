package com.mastery.payment.component;

import com.mastery.payment.configuration.ConfigLoader;
import com.pengrad.telegrambot.TelegramBot;

public class BotClient {
    private static volatile TelegramBot bot;

    public static TelegramBot getInstance() {
        if (bot == null) {
            synchronized (TelegramBot.class) {
                if (bot == null) {
                    String token = ConfigLoader.getInstance().getAsString("telegram.bot.token");
                    bot = new TelegramBot(token);
                }
            }
        }

        return bot;
    }
}
