package com.mastery.payment.component;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Slf4j
public class ExecutionTimeAspect {

    @Around("@annotation(com.mastery.payment.annotation.ExecutionTimeLogger)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        String methodName = joinPoint.getSignature().getName();
        try {
            long start = System.currentTimeMillis();
            Object proceed = joinPoint.proceed();
            long executionTime = System.currentTimeMillis() - start;
            log.info("[{}] [{}] executed in {} ms",
                    joinPoint.getSignature().getDeclaringType().getCanonicalName(),
                    methodName,
                    executionTime);

            return proceed;
        } catch (Exception e) {
            log.error("{} [failed]: {}", methodName, e.getMessage());
        }

        return null;
    }
}
