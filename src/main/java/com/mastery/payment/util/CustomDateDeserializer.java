package com.mastery.payment.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.mastery.payment.configuration.Constant;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.LocalDateTime;

public class CustomDateDeserializer extends JsonDeserializer<LocalDateTime> {
    public CustomDateDeserializer() {
    }

    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String dateString = jsonParser.getText();
        return StringUtils.isEmpty(dateString)
                ? null
                : LocalDateTime.parse(dateString, Constant.DateTimeFmt.yyyyMMdd_HHmmss);
    }
}
