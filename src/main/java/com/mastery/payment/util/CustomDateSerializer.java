package com.mastery.payment.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.mastery.payment.configuration.Constant;

import java.io.IOException;
import java.time.LocalDateTime;

public class CustomDateSerializer extends JsonSerializer<LocalDateTime> {
    public CustomDateSerializer() {
    }

    public void serialize(LocalDateTime localDateTime,
                          JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        String formattedDate = Constant.DateTimeFmt.yyyyMMdd_HHmmss.format(localDateTime);
        jsonGenerator.writeString(formattedDate);
    }
}