package com.mastery.payment.util;

import lombok.extern.slf4j.Slf4j;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Slf4j
public class CurrencyNormalizer {
    private static final Map<Character, Long> MULTIPLIERS = new HashMap<>();
    private static final long ROUNDING_UNIT = 500L; // Smallest currency unit in VND

    static {
        MULTIPLIERS.put('m', 1_000_000L); // 1 million
        MULTIPLIERS.put('M', 1_000_000L);
        MULTIPLIERS.put('k', 1_000L);     // 1 thousand
        MULTIPLIERS.put('K', 1_000L);
        // Extend here for more formats if needed
    }

    public static BigInteger normalizeCurrency(String currency) {
        if (currency == null || currency.isEmpty()) {
            throw new IllegalArgumentException("Invalid currency format");
        }

        currency = currency.replace(',', '.').trim(); // Normalize decimal separator

        if (currency.split("\\.").length > 2) {
            currency = currency.replace(".", "");
        }

        int length = currency.length();
        int endIndex = length - 1;
        char lastChar = currency.charAt(endIndex);

        Long multiplier = MULTIPLIERS.get(lastChar);
        if (multiplier != null) {
            endIndex--; // Adjust end index to exclude suffix
        } else {
            multiplier = 1L; // Default multiplier if no suffix is found
        }

        double value;
        try {
            value = Double.parseDouble(currency.substring(0, endIndex + 1));
        } catch (NumberFormatException e) {
            log.error("[normalizeCurrency] failed to parse currency: {}", e.getMessage());
            throw new IllegalArgumentException("Invalid numeric format: " + currency);
        }

        long result = (long) (value * multiplier);
        return roundToNearestVND(result);
    }

    private static BigInteger roundToNearestVND(long amount) {
        long remainder = amount % ROUNDING_UNIT;
        if (remainder < ROUNDING_UNIT / 2) {
            return BigInteger.valueOf(amount - remainder); // Round down
        } else {
            return BigInteger.valueOf(amount + (ROUNDING_UNIT - remainder)); // Round up
        }
    }

    public static String formatCurrency(String amount) {
        amount = amount
                .replace(" ₫", "")
                .replace(",", "")
                .split("\\.")[0];
        long value = Long.parseLong(amount);

        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.GERMANY);
        symbols.setGroupingSeparator('.');

        DecimalFormat formatter = new DecimalFormat("#,###", symbols);
        return formatter.format(value);
    }

    public static void main(String[] args) {
        System.out.println(normalizeCurrency("5.231.000")); // 5231000
        System.out.println(formatCurrency(String.valueOf(normalizeCurrency("18m")))); // 18000000
        System.out.println(normalizeCurrency("18M")); // 18000000
        System.out.println(normalizeCurrency("18k")); // 18000
        System.out.println(normalizeCurrency("100000")); // 100000
        System.out.println(normalizeCurrency("18.5m")); // 18500000
        System.out.println(normalizeCurrency("18,5m")); // 18500000
        System.out.println(normalizeCurrency("18.499m")); // 18499500 (rounded down)
        System.out.println(formatCurrency(String.valueOf(normalizeCurrency("18.501m")))); // 18500000 (rounded up)
        System.out.println(normalizeCurrency("18x")); // Throws exception (unsupported format)
    }
}
