package com.mastery.payment;

import com.mastery.payment.component.BotClient;
import com.mastery.payment.configuration.ConfigLoader;
import com.mastery.payment.model.CellByMonthAndUser;
import com.mastery.payment.model.GlobalCache;
import com.mastery.payment.model.PercentageByUser;
import com.mastery.payment.security.ContextInitializer;
import jakarta.annotation.PostConstruct;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAspectJAutoProxy
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class PaymentApplication {
    //com.pengrad.telegrambot.UpdatesListener updatesListener;

    public static void main(String[] args) {
        //String configLocation = String.format("conf/application-%s.properties", ConfigLoader.getInstance().getProfile());
        //System.setProperty("spring.config.location", configLocation);

        SpringApplication application = new SpringApplication(PaymentApplication.class);
        //application.addInitializers(new ContextInitializer());
        application.run(args);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            BotClient.getInstance().removeGetUpdatesListener();
            BotClient.getInstance().shutdown();
        }));
    }

    @PostConstruct
    public void init() {
        //CellByMonthAndUser.getInstance().init();
        //PercentageByUser.getInstance().init();
        //GlobalCache.getInstance().init();

        //BotClient.getInstance().setUpdatesListener(updatesListener);
    }

}
